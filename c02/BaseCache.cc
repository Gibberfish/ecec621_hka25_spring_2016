#include <iostream>
#include "BaseCache.h"
using namespace std;

//WRITE ME
//Default constructor to set everything to '0'
BaseCache::BaseCache() {}

//WRITE ME
//Constructor to initialize cache parameters, create the cache and clears it
BaseCache::BaseCache(uint32_t _cacheSize, uint32_t _associativity, uint32_t _blockSize) {
}

//WRITE ME
//Set cache base parameters
void BaseCache::setCacheSize(uint32_t _cacheSize) {}
void BaseCache::setAssociativity(uint32_t _associativity) {}
void BaseCache::setBlockSize(uint32_t _blockSize) {}

//WRITE ME
//Get cache base parameters
uint32_t BaseCache::getCacheSize() {return 0;}
uint32_t BaseCache::getAssociativity() {return 0;}
uint32_t BaseCache::getBlockSize() {return 0;}

//WRITE ME
//Get cache access statistics
uint32_t BaseCache::getReadHits() { return 0;}
uint32_t BaseCache::getReadMisses() { return 0;}
uint32_t BaseCache::getWriteHits() {return 0;}
uint32_t BaseCache::getWriteMisses() {return 0;}
double BaseCache::getReadHitRate() {return 0;}
double BaseCache::getReadMissRate() {return 0;}
double BaseCache::getWriteHitRate() {return 0;}
double BaseCache::getWriteMissRate() {return 0;}
double BaseCache::getOverallHitRate() {return 0;}
double BaseCache::getOverallMissRate() {return 0;}

//WRITE ME
//Initialize cache derived parameters
void BaseCache::initDerivedParams() {
}

//WRITE ME
//Reset cache access statistics
void BaseCache::resetStats() {
}

//WRITE ME
//Create cache and clear it
void BaseCache::createCache() {
}

//WRITE ME
//Reset cache
void BaseCache::clearCache() {
}

//WRITE ME
//Read data
//return true if it was a hit, false if it was a miss
//data is only valid if it was a hit, input data pointer
//is not updated upon miss. Make sure to update LRU stack
//bits. You can choose to separate the LRU bits update into
// a separate function that can be used from both read() and write().
bool BaseCache::read(uint32_t addr, uint32_t *data) {
    return 1;
}

//WRITE ME
//Write data
//Function returns write hit or miss status. 
bool BaseCache::write(uint32_t addr, uint32_t data) {
    return 1;
}

//WRITE ME
//Destructor to free all allocated memeroy.
BaseCache::~BaseCache() {
}
